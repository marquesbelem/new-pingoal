﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ObjInstance
{
    public string nameSpriteLoad; //caminho/nome das imagens que vai ter que carregar
    public string key; //nome da chave do PlayerPrefs
}

public class SelectInstance : MonoBehaviour
{

    [SerializeField]
    private Sprite[] listSpriteChange;
    private Image img;

    [SerializeField]
    private int indexSelect;

    [SerializeField]
    private string[] listNamesAcquired; //Ex: CompradoPalhetaAzul

    public ObjInstance[] listInstanceLoad;

    [SerializeField]
    private string SpriteDefault0; //Nome da primeira imagem padrão
    [SerializeField]
    private string SpriteDefault1; //Nome da segunda imagem padrão

    [SerializeField]
    private Button buttonPrevius;
    [SerializeField]
    private Button buttonNext;
    void Start()
    {
        img = this.GetComponent<Image>();
        //AddSprites();

        listSpriteChange[0] = Resources.Load<Sprite>(SpriteDefault0);
        listSpriteChange[1] = Resources.Load<Sprite>(SpriteDefault1);
    }

    void Update()
    {
        UpdateImage();
    }

    public void NextChange()
    {
        indexSelect++;
    }

    public void PreviusChange()
    {
        indexSelect--;
    }

    void UpdateImage()
    {
        if (indexSelect == 0)
            buttonPrevius.interactable = false;
        else
            buttonPrevius.interactable = true;


        if (indexSelect > 0 && indexSelect < listSpriteChange.Length - 1)
        {
            buttonPrevius.interactable = true;
            buttonNext.interactable = true;
        }

        if (indexSelect >= listSpriteChange.Length - 1)
            buttonNext.interactable = false;
        else
            buttonNext.interactable = true;

        img.sprite = listSpriteChange[indexSelect];
    }

    void AddSprites()
    {
        for (int i = 0; i < listNamesAcquired.Length; i++)
        {
            if (PlayerPrefs.GetString(listNamesAcquired[i]) == "True")
            {
                listSpriteChange = new Sprite[listSpriteChange.Length + 1];
            }
        }
    }
}
